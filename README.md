# Gavagai Explorer Postman-Newman Tests
> Gavagai Explorer API Tests using Postman with Newman for command-line runner.

[![npm Version][npm-image]][npm-url]
[![postman Version][postman-image]][postman-url]
[![newman Version][newman-image]][newman-url]
[![newmanhtmlextra Version][newmanhtmlextra-image]][newmanhtmlextra-url]

Gavagai Explorer API Tests using Postman to make REST calls and retrive response details. This project uses Newman which is a command-line collection runner for Postman. This is then wrapped using npm with requiered dependencies as part of package.json file for smooth integration with CI/CD systems. This uses the htmlextra reporter for generating a detailed html report of test runs.

![](https://bitbucket.org/rohanganapathypattada/gavagairepoimages/raw/8335af0307106abb3c5e2d29a6a8974f28f9daa4/gavagai-explorer-postman-tests-images/apipostmannewman.png)

## Installation

OS X & Linux:

Install Brew
> 
> ```
> /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
> ```
	
Install node

> ```
>  brew install node
> ```
	
Windows:

Follow the steps to install Node: [Link](https://nodejs.org/en/download/)

## Usage example

To run the test from terminal, navigate to the project folder

> ```
> npm install
> ```

> ```
> npm api-test:qa
> ```
	
The tests can be run depending on the environment

> ```
> npm api-test:dev
> ```

> ```
> npm api-test:qa
> ```

> ```
> npm api-test:stage
> ```

> ```
> npm api-test:prod
> ```
	
Note: The tests run in the background, give it time to finish and generate the report.

## Reporting

A htmlextra report is generated after each test run

The report is located in the newman folder once the test has completed. eg: gavagai-explorer-postman-tests/newman/GavagaiAPITests-2020-04-13-00-08-45-033-0.html

The report Summary page is the dasboard that shows all the overall information of the test run

![](https://bitbucket.org/rohanganapathypattada/gavagairepoimages/raw/8335af0307106abb3c5e2d29a6a8974f28f9daa4/gavagai-explorer-postman-tests-images/postmansummary.png)

The Request can be accessed by clicking on the request button on the report. This shows all the test scenarios and test steps that are run as part of the test and also shows fail cases.

![](https://bitbucket.org/rohanganapathypattada/gavagairepoimages/raw/8335af0307106abb3c5e2d29a6a8974f28f9daa4/gavagai-explorer-postman-tests-images/postmanrequests.png)

Clicking on a test case shows all the details of request and response made during the test

![](https://bitbucket.org/rohanganapathypattada/gavagairepoimages/raw/8335af0307106abb3c5e2d29a6a8974f28f9daa4/gavagai-explorer-postman-tests-images/requestopen.png)

Failed tests can be accessed by clicking on the Failed Tests button on the report. This shows all the test cases that failed during the run. Clicking on each of the case shows appropriate details of the fail.

![](https://bitbucket.org/rohanganapathypattada/gavagairepoimages/raw/8335af0307106abb3c5e2d29a6a8974f28f9daa4/gavagai-explorer-postman-tests-images/failedopentests.png)

## Meta

Author – [@RohanGanapathy](rohanganapathy@gmail.com)

[bitbucket](https://bitbucket.org/%7B4674caf3-36f1-4934-9c9e-fb4768e30707%7D/)


[npm-image]: https://img.shields.io/badge/npm-6.14.4-orange
[npm-url]: https://github.com/npm/cli
[newman-image]: https://img.shields.io/badge/newman-green
[newman-url]: https://github.com/postmanlabs/newman
[postman-image]: https://img.shields.io/badge/postman-darkorange
[postman-url]: https://www.postman.com/
[newmanhtmlextra-image]: https://img.shields.io/badge/newman_htmlextra-darkgreen
[newmanhtmlextra-url]: https://github.com/DannyDainton/newman-reporter-htmlextra